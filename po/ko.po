# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Red Hat, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# bardisch <kwb0128@gmail.com>, 2011
# trans22 <trans22@naver.com>, 2013
# eukim <eukim@redhat.com>, 2006, 2007, 2008, 2009
# eukim <eukim@redhat.com>, 2012
# Michelle Ji Yeen Kim <mkim@redhat.com>, 2005, 2006
# Michelle J Kim <mkim@redhat.com>, 2004
# Michelle Kim <mkim@redhat.com>, 2001, 2002
# Miloslav Trmač <mitr@volny.cz>, 2011
msgid ""
msgstr ""
"Project-Id-Version: libuser 0.60\n"
"Report-Msgid-Bugs-To: http://bugzilla.redhat.com/bugzilla/\n"
"POT-Creation-Date: 2015-07-23 21:16+0200\n"
"PO-Revision-Date: 2013-04-29 04:37-0400\n"
"Last-Translator: Miloslav Trmač <mitr@volny.cz>\n"
"Language-Team: Korean (http://www.transifex.com/projects/p/fedora/language/"
"ko/)\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Zanata 3.6.2\n"

#: apps/apputil.c:197 apps/apputil.c:201
#, c-format
msgid "Failed to drop privileges.\n"
msgstr "슈퍼유저 권한을 없애는데 실패했습니다.\n"

#: apps/apputil.c:210
#, c-format
msgid "Internal error.\n"
msgstr "내부 오류가 발생했습니다.\n"

#: apps/apputil.c:231
#, c-format
msgid "%s is not authorized to change the finger info of %s\n"
msgstr "%s은(는) %s의 finger 정보를 변경할 권한이 없습니다\n"

#: apps/apputil.c:233
msgid "Unknown user context"
msgstr "알 수 없는 사용자 문맥"

#: apps/apputil.c:241
#, c-format
msgid "Can't set default context for /etc/passwd\n"
msgstr "/etc/passwd의 기본 문맥을 설정할 수 없습니다\n"

#: apps/apputil.c:250
#, c-format
msgid "Error initializing PAM.\n"
msgstr "PAM을 초기화 하는 도중 오류가 발생했습니다.\n"

#: apps/apputil.c:260 apps/apputil.c:287
#, c-format
msgid "Authentication failed for %s.\n"
msgstr "%s의 인증에 실패했습니다.\n"

#: apps/apputil.c:268
#, c-format
msgid "Internal PAM error `%s'.\n"
msgstr "내부 PAM 오류 `%s'(이)가 발생했습니다.\n"

#: apps/apputil.c:273
#, c-format
msgid "Unknown user authenticated.\n"
msgstr "알 수 없는 사용자 인증함.\n"

#: apps/apputil.c:277
#, c-format
msgid "User mismatch.\n"
msgstr "사용자가 일치하지 않습니다.\n"

#: apps/lchage.c:84 apps/lchfn.c:53 apps/lchsh.c:44 apps/lgroupadd.c:47
#: apps/lgroupdel.c:42 apps/lgroupmod.c:54 apps/lid.c:115 apps/lnewusers.c:45
#: apps/lpasswd.c:47 apps/luseradd.c:55 apps/luserdel.c:45 apps/lusermod.c:56
msgid "prompt for all information"
msgstr "모든 정보를 요청하다."

#: apps/lchage.c:86
msgid "list aging parameters for the user"
msgstr ""

#: apps/lchage.c:88
msgid "minimum days between password changes"
msgstr "최소한의 암호 유지기간"

#: apps/lchage.c:88 apps/lchage.c:90 apps/lchage.c:93 apps/lchage.c:96
#: apps/lchage.c:99 apps/lchage.c:102
msgid "DAYS"
msgstr "일"

#: apps/lchage.c:90
msgid "maximum days between password changes"
msgstr "암호 최대 유지기간"

#: apps/lchage.c:92
msgid "date of last password change in days since 1/1/70"
msgstr "일단위로 표시한 1970년 1월 1일 이후 마지막으로 암호가 변경될 날짜"

#: apps/lchage.c:95
msgid ""
"number of days after password expiration date when account is considered "
"inactive"
msgstr ""

#: apps/lchage.c:98
msgid "password expiration date in days since 1/1/70"
msgstr ""

#: apps/lchage.c:101
msgid "days before expiration to begin warning user"
msgstr ""

#: apps/lchage.c:114 apps/lid.c:130 apps/lpasswd.c:68 apps/luseradd.c:100
#: apps/luserdel.c:59 apps/lusermod.c:101
msgid "[OPTION...] user"
msgstr "[옵션...] 사용자"

#: apps/lchage.c:117 apps/lchfn.c:70 apps/lchsh.c:58 apps/lgroupadd.c:65
#: apps/lgroupdel.c:56 apps/lgroupmod.c:85 apps/lid.c:133 apps/lnewusers.c:66
#: apps/lpasswd.c:71 apps/luseradd.c:103 apps/luserdel.c:62
#: apps/lusermod.c:104
#, c-format
msgid "Error parsing arguments: %s.\n"
msgstr "인수를 처리(parsing)하는 도중 오류 발생: %s.\n"

#: apps/lchage.c:126 apps/lpasswd.c:85 apps/luseradd.c:118 apps/luserdel.c:70
#: apps/lusermod.c:115
#, c-format
msgid "No user name specified.\n"
msgstr "사용자 이름이 지정되지 않았습니다.\n"

#: apps/lchage.c:138 apps/lchfn.c:103 apps/lchsh.c:91 apps/lgroupadd.c:102
#: apps/lgroupdel.c:77 apps/lgroupmod.c:119 apps/lid.c:179 apps/lnewusers.c:79
#: apps/lpasswd.c:97 apps/luseradd.c:143 apps/luserdel.c:81
#: apps/lusermod.c:157 samples/enum.c:56 samples/testuser.c:71
#, c-format
msgid "Error initializing %s: %s.\n"
msgstr "%s(을)를 초기화하는 도중 오류 발생: %s.\n"

#: apps/lchage.c:147 apps/lchfn.c:114 apps/lchsh.c:102 apps/lpasswd.c:148
#: apps/luserdel.c:89 apps/lusermod.c:171
#, c-format
msgid "User %s does not exist.\n"
msgstr "%s 사용자가 존재하지 않습니다.\n"

#: apps/lchage.c:160
#, c-format
msgid "Account is locked.\n"
msgstr "계정이 정지되었습니다.\n"

#: apps/lchage.c:162
#, c-format
msgid "Account is not locked.\n"
msgstr "계정이 정지되지 않았습니다.\n"

#: apps/lchage.c:166
#, c-format
msgid "Minimum:\t%ld\n"
msgstr "최소값:\t%ld\n"

#: apps/lchage.c:170
#, c-format
msgid "Maximum:\t%ld\n"
msgstr "최대값:\t%ld\n"

#: apps/lchage.c:172
#, c-format
msgid "Maximum:\tNone\n"
msgstr ""

#: apps/lchage.c:175
#, c-format
msgid "Warning:\t%ld\n"
msgstr "경고:\t%ld\n"

#: apps/lchage.c:180
#, c-format
msgid "Inactive:\t%ld\n"
msgstr "비활성:\t%ld\n"

#: apps/lchage.c:182
#, c-format
msgid "Inactive:\tNever\n"
msgstr ""

#: apps/lchage.c:186 apps/lchage.c:194 apps/lchage.c:204
msgid "Must change password on next login"
msgstr ""

#: apps/lchage.c:188 apps/lchage.c:196 apps/lchage.c:206 apps/lchage.c:215
msgid "Never"
msgstr "없음"

#: apps/lchage.c:191
#, c-format
msgid "Last Change:\t%s\n"
msgstr "최종 변경:\t%s\n"

#: apps/lchage.c:201
#, c-format
msgid "Password Expires:\t%s\n"
msgstr "암호 만기일:\t%s\n"

#: apps/lchage.c:213
#, c-format
msgid "Password Inactive:\t%s\n"
msgstr "암호 비활성:\t%s\n"

#: apps/lchage.c:219
#, c-format
msgid "Account Expires:\t%s\n"
msgstr "계정 만기일:\t%s\n"

#: apps/lchage.c:240
#, c-format
msgid "Failed to modify aging information for %s: %s\n"
msgstr "%s의 사용 기한(aging) 정보를 수정하는데 실패했습니다: %s\n"

#: apps/lchfn.c:67 apps/lchsh.c:55
msgid "[OPTION...] [user]"
msgstr "[OPTION...] [사용자]"

#: apps/lchfn.c:86 apps/lchsh.c:74 apps/lid.c:164
#, c-format
msgid "No user name specified, no name for uid %d.\n"
msgstr "사용자 이름과 uid %d에 대한 이름이 지정되지 않았습니다.\n"

#: apps/lchfn.c:96
#, c-format
msgid "Changing finger information for %s.\n"
msgstr "%s의 finger (핑거) 정보를 변경합니다.\n"

#: apps/lchfn.c:136
msgid "Full Name"
msgstr "성명"

#: apps/lchfn.c:146
msgid "Surname"
msgstr "성"

#: apps/lchfn.c:157
msgid "Given Name"
msgstr "이름"

#: apps/lchfn.c:167
msgid "Office"
msgstr "직장"

#: apps/lchfn.c:176
msgid "Office Phone"
msgstr "직장 전화 번호"

#: apps/lchfn.c:185
msgid "Home Phone"
msgstr "집 전화 번호"

#: apps/lchfn.c:195
msgid "E-Mail Address"
msgstr "이메일 주소"

#: apps/lchfn.c:208
#, c-format
msgid "Finger information not changed:  input error.\n"
msgstr "finger 정보가 변경되지 않았습니다:  입력 오류.\n"

#: apps/lchfn.c:270
msgid "Finger information changed.\n"
msgstr "finger 정보가 변경되었습니다.\n"

#: apps/lchfn.c:273
#, c-format
msgid "Finger information not changed: %s.\n"
msgstr "finger 정보가 변경되지 않았습니다: %s.\n"

#: apps/lchsh.c:84
#, c-format
msgid "Changing shell for %s.\n"
msgstr "%s 용 쉘을 변경합니다.\n"

#: apps/lchsh.c:114
msgid "New Shell"
msgstr "새로운 쉘"

#: apps/lchsh.c:121 apps/lchsh.c:136
#, c-format
msgid "Shell not changed: %s\n"
msgstr "쉘이 변경되지 않았습니다: %s\n"

#: apps/lchsh.c:133
msgid "Shell changed.\n"
msgstr "쉘이 변경되었습니다.\n"

#: apps/lgroupadd.c:49
msgid "gid for new group"
msgstr ""

#: apps/lgroupadd.c:49 apps/lgroupmod.c:56 apps/lpasswd.c:56 apps/lpasswd.c:59
#: apps/luseradd.c:67 apps/lusermod.c:66 apps/lusermod.c:68
msgid "NUM"
msgstr ""

#: apps/lgroupadd.c:51
msgid "create a system group"
msgstr ""

#: apps/lgroupadd.c:62 apps/lgroupdel.c:53 apps/lgroupmod.c:82
msgid "[OPTION...] group"
msgstr "[옵션...] 그룹"

#: apps/lgroupadd.c:74 apps/lgroupdel.c:65 apps/lgroupmod.c:93
#, c-format
msgid "No group name specified.\n"
msgstr "그룹 이름이 지정되지 않았습니다.\n"

#: apps/lgroupadd.c:87 apps/lgroupmod.c:105 apps/lnewusers.c:172
#: apps/luseradd.c:164 apps/lusermod.c:127
#, c-format
msgid "Invalid group ID %s\n"
msgstr "잘못된 그룹 ID %s\n"

#: apps/lgroupadd.c:119
#, c-format
msgid "Group creation failed: %s\n"
msgstr "그룹 생성에 실패했습니다: %s\n"

#: apps/lgroupdel.c:85 apps/lgroupmod.c:132 apps/lpasswd.c:153
#, c-format
msgid "Group %s does not exist.\n"
msgstr "%s 그룹이 존재하지 않습니다.\n"

#: apps/lgroupdel.c:91
#, c-format
msgid "Group %s could not be deleted: %s\n"
msgstr "%s 그룹을 삭제할 수 없습니다: %s\n"

#: apps/lgroupmod.c:56
msgid "set GID for group"
msgstr ""

#: apps/lgroupmod.c:58
msgid "change group to have given name"
msgstr ""

#: apps/lgroupmod.c:58
msgid "NAME"
msgstr ""

#: apps/lgroupmod.c:60 apps/luseradd.c:75
msgid "plaintext password for use with group"
msgstr ""

#: apps/lgroupmod.c:60 apps/lgroupmod.c:62 apps/lgroupmod.c:64
#: apps/lgroupmod.c:66 apps/lgroupmod.c:68 apps/lgroupmod.c:70
#: apps/lpasswd.c:51 apps/lpasswd.c:53 apps/luseradd.c:59 apps/luseradd.c:61
#: apps/luseradd.c:63 apps/luseradd.c:65 apps/luseradd.c:69 apps/luseradd.c:75
#: apps/luseradd.c:77 apps/luseradd.c:79 apps/luseradd.c:81 apps/luseradd.c:83
#: apps/luseradd.c:85 apps/luseradd.c:87 apps/luseradd.c:89 apps/lusermod.c:58
#: apps/lusermod.c:60 apps/lusermod.c:64 apps/lusermod.c:70 apps/lusermod.c:72
#: apps/lusermod.c:74 apps/lusermod.c:80 apps/lusermod.c:82 apps/lusermod.c:84
#: apps/lusermod.c:86 apps/lusermod.c:88 apps/lusermod.c:90
msgid "STRING"
msgstr ""

#: apps/lgroupmod.c:62 apps/luseradd.c:77
msgid "pre-hashed password for use with group"
msgstr ""

#: apps/lgroupmod.c:64
msgid "list of administrators to add"
msgstr ""

#: apps/lgroupmod.c:66
msgid "list of administrators to remove"
msgstr ""

#: apps/lgroupmod.c:68
msgid "list of group members to add"
msgstr ""

#: apps/lgroupmod.c:70
msgid "list of group members to remove"
msgstr ""

#: apps/lgroupmod.c:71
msgid "lock group"
msgstr ""

#: apps/lgroupmod.c:72
msgid "unlock group"
msgstr ""

#: apps/lgroupmod.c:125 apps/lusermod.c:164
#, c-format
msgid "Both -L and -U specified.\n"
msgstr "-L 와 -U 이 함께 지정되었습니다.\n"

#: apps/lgroupmod.c:139 apps/lgroupmod.c:148
#, c-format
msgid "Failed to set password for group %s: %s\n"
msgstr "%s 그룹의 암호를 설정하는데 실패했습니다: %s\n"

#: apps/lgroupmod.c:157
#, c-format
msgid "Group %s could not be locked: %s\n"
msgstr "%s 그룹을 잠글 수 없습니다: %s\n"

#: apps/lgroupmod.c:166
#, c-format
msgid "Group %s could not be unlocked: %s\n"
msgstr "%s 그룹 잠금을 해제할 수 없습니다: %s\n"

#: apps/lgroupmod.c:242 apps/lgroupmod.c:257
#, c-format
msgid "Group %s could not be modified: %s\n"
msgstr "%s 그룹을 편집할 수 없습니다: %s\n"

#: apps/lid.c:42 apps/lid.c:74 apps/lid.c:188
#, c-format
msgid "Error looking up %s: %s\n"
msgstr "%s(을)를 조회하는 도중 오류 발생: %s.\n"

#: apps/lid.c:117
msgid ""
"list members of a named group instead of the group memberships for the named "
"user"
msgstr ""

#: apps/lid.c:120
msgid "only list membership information by name, and not UID/GID"
msgstr ""

#: apps/lid.c:146
#, c-format
msgid "No group name specified, using %s.\n"
msgstr "%s를 사용하여 그룹 이름이 지정되지 않았습니다.\n"

#: apps/lid.c:150
#, c-format
msgid "No group name specified, no name for gid %d.\n"
msgstr "그룹 이름과 gid %d에 대한 이름이 지정되지 않았습니다.\n"

#: apps/lid.c:160
#, c-format
msgid "No user name specified, using %s.\n"
msgstr "%s를 사용하여 사용자 이름이 지정되지 않았습니다.\n"

#: apps/lid.c:192
#, c-format
msgid "%s does not exist\n"
msgstr "%s이(가) 존재하지 않습니다\n"

#: apps/lnewusers.c:47
msgid "file with user information records"
msgstr ""

#: apps/lnewusers.c:47
msgid "PATH"
msgstr ""

#: apps/lnewusers.c:49
msgid "don't create home directories"
msgstr ""

#: apps/lnewusers.c:51
msgid "don't create mail spools"
msgstr ""

#: apps/lnewusers.c:63
msgid "[OPTION...]"
msgstr "[OPTION...]"

#: apps/lnewusers.c:88
#, c-format
msgid "Error opening `%s': %s.\n"
msgstr "`%s'(을)를 여는 도중 오류 발생: %s.\n"

#: apps/lnewusers.c:118
#, c-format
msgid "Error creating account for `%s': line improperly formatted.\n"
msgstr ""
"`%s'의 계정을 생성하는데 오류가 발생했습니다: 라인이 부적절하게 포맷되었습니"
"다.\n"

#: apps/lnewusers.c:129 apps/luseradd.c:128 apps/lusermod.c:142
#, c-format
msgid "Invalid user ID %s\n"
msgstr "잘못된 사용자 ID %s\n"

#: apps/lnewusers.c:136
msgid "Refusing to create account with UID 0.\n"
msgstr "UID 0를 가진 계정을 생성하기를 거부합니다.\n"

#: apps/lnewusers.c:206
#, c-format
msgid "Error creating group for `%s' with GID %jd: %s\n"
msgstr "`%s'에 GID %jd를 사용하여 그룹을 생성하는데 오류 발생: : %s\n"

#: apps/lnewusers.c:246
#, c-format
msgid "Refusing to use dangerous home directory `%s' for %s by default\n"
msgstr ""
"%s에 대해 기본값으로 안전하지 않은 `%s' 홈 디렉토리 사용을 거부합니다\n"

#: apps/lnewusers.c:257
#, c-format
msgid "Error creating home directory for %s: %s\n"
msgstr "%s의 홈 디렉토리를 생성하는 도중 오류가 발생했습니다: %s\n"

#: apps/lnewusers.c:270
#, c-format
msgid "Error creating mail spool for %s: %s\n"
msgstr ""

#: apps/lnewusers.c:285
#, c-format
msgid "Error setting initial password for %s: %s\n"
msgstr "`%s'의 초기 암호를 설정하는 도중 오류가 발생했습니다: %s\n"

#: apps/lnewusers.c:295
#, c-format
msgid "Error creating user account for %s: %s\n"
msgstr "%s의 사용자 계정을 생성하는 도중 오류 발생: %s.\n"

#: apps/lpasswd.c:49
msgid "set group password instead of user password"
msgstr ""

#: apps/lpasswd.c:51
msgid "new plain password"
msgstr ""

#: apps/lpasswd.c:53
msgid "new crypted password"
msgstr ""

#: apps/lpasswd.c:55
msgid "read new plain password from given descriptor"
msgstr ""

#: apps/lpasswd.c:58
msgid "read new crypted password from given descriptor"
msgstr ""

#: apps/lpasswd.c:83
#, c-format
msgid "Changing password for %s.\n"
msgstr "`%s'의 암호를 변경합니다.\n"

#: apps/lpasswd.c:111
msgid "New password"
msgstr "새 암호"

#: apps/lpasswd.c:114
msgid "New password (confirm)"
msgstr "새 암호 (확인)"

#: apps/lpasswd.c:128
#, c-format
msgid "Passwords do not match, try again.\n"
msgstr "암호가 일치하지 않습니다, 다시 입력해 주십시오.\n"

#: apps/lpasswd.c:133
#, c-format
msgid "Password change canceled.\n"
msgstr "암호 변경이 취소되었습니다.\n"

#: apps/lpasswd.c:165 apps/lpasswd.c:182
#, c-format
msgid "Error reading from file descriptor %d.\n"
msgstr "파일 기술어 %d를 읽지 못했습니다.\n"

#: apps/lpasswd.c:203 apps/luseradd.c:302 apps/luseradd.c:311
#, c-format
msgid "Error setting password for user %s: %s.\n"
msgstr "%s 사용자의 암호를 설정하는데 오류 발생: %s.\n"

#: apps/lpasswd.c:212
#, c-format
msgid "Error setting password for group %s: %s.\n"
msgstr "`%s' 그룹의 암호를 설정하는 도중 오류 발생: %s.\n"

#: apps/lpasswd.c:224
#, c-format
msgid "Password changed.\n"
msgstr "암호가 변경되었습니다.\n"

#: apps/luseradd.c:57
msgid "create a system user"
msgstr ""

#: apps/luseradd.c:59
msgid "GECOS information for new user"
msgstr ""

#: apps/luseradd.c:61
msgid "home directory for new user"
msgstr ""

#: apps/luseradd.c:63
msgid "directory with files for the new user"
msgstr ""

#: apps/luseradd.c:65
msgid "shell for new user"
msgstr ""

#: apps/luseradd.c:67
msgid "uid for new user"
msgstr ""

#: apps/luseradd.c:69
msgid "group for new user"
msgstr ""

#: apps/luseradd.c:71
msgid "don't create home directory for user"
msgstr ""

#: apps/luseradd.c:73
msgid "don't create group with same name as user"
msgstr ""

#: apps/luseradd.c:79
msgid "common name for new user"
msgstr ""

#: apps/luseradd.c:81
msgid "given name for new user"
msgstr ""

#: apps/luseradd.c:83
msgid "surname for new user"
msgstr ""

#: apps/luseradd.c:85
msgid "room number for new user"
msgstr ""

#: apps/luseradd.c:87
msgid "telephone number for new user"
msgstr ""

#: apps/luseradd.c:89
msgid "home telephone number for new user"
msgstr ""

#: apps/luseradd.c:189
#, c-format
msgid "Group %jd does not exist\n"
msgstr "%jd 그룹이 존재하지 않습니다\n"

#: apps/luseradd.c:207 apps/luseradd.c:220
#, c-format
msgid "Error creating group `%s': %s\n"
msgstr "`%s' 그룹을 생성하는 도중 오류 발생: %s\n"

#: apps/luseradd.c:260
#, c-format
msgid "Account creation failed: %s.\n"
msgstr "계정 생성에 실패함: %s.\n"

#: apps/luseradd.c:283
#, c-format
msgid "Error creating %s: %s.\n"
msgstr "%s(을)를 생성하는 도중 오류 발생: %s.\n"

#: apps/luseradd.c:290
#, c-format
msgid "Error creating mail spool: %s\n"
msgstr ""

#: apps/luserdel.c:47
msgid "don't remove the user's private group, if the user has one"
msgstr ""

#: apps/luserdel.c:50
msgid "remove the user's home directory"
msgstr ""

#: apps/luserdel.c:94
#, c-format
msgid "User %s could not be deleted: %s.\n"
msgstr "%s 사용자를 삭제할 수 없습니다: %s.\n"

#: apps/luserdel.c:108
#, c-format
msgid "%s did not have a gid number.\n"
msgstr "%s(은)는 gid number를 갖고 있지 않습니다.\n"

#: apps/luserdel.c:114
#, c-format
msgid "No group with GID %jd exists, not removing.\n"
msgstr "GID %jd라는 그룹이 존재하지 않으므로, 삭제할 수 없습니다.\n"

#: apps/luserdel.c:120
#, c-format
msgid "Group with GID %jd did not have a group name.\n"
msgstr "GID %jd 라는 그룹에는 이름이 지정되어 있지 않습니다.\n"

#: apps/luserdel.c:126
#, c-format
msgid "Group %s could not be deleted: %s.\n"
msgstr "%s 그룹은 삭제할 수 없습니다: %s.\n"

#: apps/luserdel.c:139
#, fuzzy, c-format
msgid "Error removing home directory: %s.\n"
msgstr "사용자의 홈 디렉토리를 삭제하는 중 오류 발생"

#: apps/luserdel.c:145
#, c-format
msgid "Error removing mail spool: %s"
msgstr ""

#: apps/lusermod.c:58
msgid "GECOS information"
msgstr ""

#: apps/lusermod.c:60
msgid "home directory"
msgstr ""

#: apps/lusermod.c:62
msgid "move home directory contents"
msgstr ""

#: apps/lusermod.c:64
msgid "set shell for user"
msgstr ""

#: apps/lusermod.c:66
msgid "set UID for user"
msgstr ""

#: apps/lusermod.c:68
msgid "set primary GID for user"
msgstr ""

#: apps/lusermod.c:70
msgid "change login name for user"
msgstr ""

#: apps/lusermod.c:72
msgid "plaintext password for the user"
msgstr ""

#: apps/lusermod.c:74
msgid "pre-hashed password for the user"
msgstr ""

#: apps/lusermod.c:75
msgid "lock account"
msgstr ""

#: apps/lusermod.c:78
msgid "unlock account"
msgstr ""

#: apps/lusermod.c:80
msgid "set common name for user"
msgstr ""

#: apps/lusermod.c:82
msgid "set given name for user"
msgstr ""

#: apps/lusermod.c:84
msgid "set surname for user"
msgstr ""

#: apps/lusermod.c:86
msgid "set room number for user"
msgstr ""

#: apps/lusermod.c:88
msgid "set telephone number for user"
msgstr ""

#: apps/lusermod.c:90
msgid "set home telephone number for user"
msgstr ""

#: apps/lusermod.c:180 apps/lusermod.c:193
#, c-format
msgid "Failed to set password for user %s: %s.\n"
msgstr "%s 사용자의 암호를 설정하는데 실패함: %s.\n"

#: apps/lusermod.c:203
#, c-format
msgid "User %s could not be locked: %s.\n"
msgstr "%s 사용자를 정지시킬 수 없습니다: %s.\n"

#: apps/lusermod.c:211
#, c-format
msgid "User %s could not be unlocked: %s.\n"
msgstr "%s 사용자의 정지를 해제할 수 없습니다: %s.\n"

#: apps/lusermod.c:232
#, c-format
msgid "Warning: Group with ID %jd does not exist.\n"
msgstr "경고: ID %jd로된 그룹이 존재하지 않습니다.\n"

#: apps/lusermod.c:275
#, c-format
msgid "User %s could not be modified: %s.\n"
msgstr "%s 사용자를 편집할 수 없습니다: %s.\n"

#: apps/lusermod.c:326
#, c-format
msgid "Group %s could not be modified: %s.\n"
msgstr "%s 그룹을 편집할 수 없습니다: %s.\n"

#: apps/lusermod.c:342
#, c-format
msgid "No old home directory for %s.\n"
msgstr "%s의 이전 홈 디렉토리가 없습니다.\n"

#: apps/lusermod.c:347
#, c-format
msgid "No new home directory for %s.\n"
msgstr "%s의 새로운 홈 디렉토리가 없습니다.\n"

#: apps/lusermod.c:353
#, c-format
msgid "Error moving %s to %s: %s.\n"
msgstr "%s에서 %s(으)로 이동하는 도중 오류 발생: %s.\n"

#: lib/config.c:128
#, c-format
msgid "could not open configuration file `%s': %s"
msgstr "설정 파일 `%s'(을)를 열 수 없습니다: %s"

#: lib/config.c:134
#, c-format
msgid "could not stat configuration file `%s': %s"
msgstr "설정 파일 `%s'에 stat 명령 실행하는데 실패했습니다: %s"

#: lib/config.c:143
#, c-format
msgid "configuration file `%s' is too large"
msgstr "설정 파일 `%s' 용량이 너무 큽니다"

#: lib/config.c:159
#, c-format
msgid "could not read configuration file `%s': %s"
msgstr "설정 파일 `%s'(을)를 읽을 수 없습니다: %s"

#: lib/error.c:62
msgid "success"
msgstr "성공"

#: lib/error.c:64
msgid "module disabled by configuration"
msgstr "설정에 의해 모듈을 사용할 수 없음"

#: lib/error.c:66
msgid "generic error"
msgstr "일반적인 오류"

#: lib/error.c:68
msgid "not enough privileges"
msgstr "수퍼유저 권한이 충분치 않음"

#: lib/error.c:70
msgid "access denied"
msgstr "접근이 거부됨"

#: lib/error.c:72
msgid "bad user/group name"
msgstr "사용자/그룹의 이름이 잘못됨"

#: lib/error.c:74
msgid "bad user/group id"
msgstr "사용자/그룹의 id가 잘못됨"

#: lib/error.c:76
msgid "user/group name in use"
msgstr "사용자/그룹의 이름이 이미 사용되고 있음"

#: lib/error.c:78
msgid "user/group id in use"
msgstr "사용자/그룹의 id가 이미 사용되고 있음"

#: lib/error.c:80
msgid "error manipulating terminal attributes"
msgstr "터미널 속성을 설정하는 도중 오류 발생"

#: lib/error.c:82
msgid "error opening file"
msgstr "파일을 여는 도중 오류 발생"

#: lib/error.c:84
msgid "error locking file"
msgstr "파일을 잠그는 도중 오류 발생"

#: lib/error.c:86
msgid "error statting file"
msgstr "파일의 상태를 확인하는 도중 오류 발생"

#: lib/error.c:88
msgid "error reading file"
msgstr "파일을 읽는 도중 오류 발생"

#: lib/error.c:90
msgid "error writing to file"
msgstr "파일에 기록하는 도중 오류 발생"

#: lib/error.c:92
msgid "data not found in file"
msgstr "파일에서 자료(data)를 찾을 수 없음"

#: lib/error.c:94
msgid "internal initialization error"
msgstr "내부 초기화 오류"

#: lib/error.c:96
msgid "error loading module"
msgstr "모듈을 적재하는 도중 오류 발생"

#: lib/error.c:98
msgid "error resolving symbol in module"
msgstr "모듈에서 심볼을 처리하는 도중 오류 발생"

#: lib/error.c:100
msgid "library/module version mismatch"
msgstr "라이브러리/모듈 버젼이 일치하지 않음"

#: lib/error.c:102
msgid "unlocking would make the password field empty"
msgstr "잠금 해제하시면 암호 입력란이 비워집니다"

#: lib/error.c:105
msgid "invalid attribute value"
msgstr "올바르지 않은 속성 "

#: lib/error.c:107
msgid "invalid module combination"
msgstr "올바르지 않은 모듈 "

#: lib/error.c:109
msgid "user's home directory not owned by them"
msgstr ""

#: lib/error.c:115
msgid "unknown error"
msgstr "알 수 없는 오류"

#: lib/misc.c:240
msgid "invalid number"
msgstr "잘못된 번호 "

#: lib/misc.c:254
msgid "invalid ID"
msgstr "잘못된 ID "

#: lib/modules.c:61
#, c-format
msgid "no initialization function %s in `%s'"
msgstr "`%s'에 초기화 기능 %s(이)가 없습니다"

#: lib/modules.c:79
#, c-format
msgid "module version mismatch in `%s'"
msgstr "`%s'의 모듈 버젼이 일치하지 않습니다"

#: lib/modules.c:92
#, c-format
msgid "module `%s' does not define `%s'"
msgstr "`%s'  모듈은 `%s'을(를) 정의하지 않습니다"

#: lib/prompt.c:88
msgid "error reading terminal attributes"
msgstr "터미널 속성을 읽는 도중 오류 발생"

#: lib/prompt.c:95 lib/prompt.c:107
msgid "error setting terminal attributes"
msgstr "터미널 속성을 설정하는 도중 오류 발생"

#: lib/prompt.c:101
msgid "error reading from terminal"
msgstr "터미널로 부터 읽는 도중 오류 발생"

#: lib/user.c:218
msgid "name is not set"
msgstr "이름이 설정되지 않았습니다."

#: lib/user.c:223
msgid "name is too short"
msgstr "이름이 너무 짧습니다."

#: lib/user.c:228
#, c-format
msgid "name is too long (%zu > %d)"
msgstr "이름이 너무 깁니다 (%zu > %d)"

#: lib/user.c:235
msgid "name contains non-ASCII characters"
msgstr "이름에 아스키 (ASCII)가 아닌 문자가 들어 있습니다."

#: lib/user.c:242
msgid "name contains control characters"
msgstr "이름에 제어 문자가 포함되어 있습니다."

#: lib/user.c:249
msgid "name contains whitespace"
msgstr "이름에 공백이 포함되어 있습니다."

#: lib/user.c:261
msgid "name starts with a hyphen"
msgstr "이름이 하이픈(연결 부호)로 시작합니다"

#: lib/user.c:272
#, c-format
msgid "name contains invalid char `%c'"
msgstr "이름에 잘못된 문자 `%c'가 있습니다."

#: lib/user.c:308 lib/user.c:360
#, c-format
msgid "user %s has no UID"
msgstr "사용자 %s는 UID가 없습니다"

#: lib/user.c:310
#, c-format
msgid "user %s not found"
msgstr ""

#: lib/user.c:333 lib/user.c:361
#, c-format
msgid "group %s has no GID"
msgstr "그룹 %s는 GID가 없습니다"

#: lib/user.c:335
#, c-format
msgid "group %s not found"
msgstr ""

#: lib/user.c:355
#, c-format
msgid "user %jd has no name"
msgstr "사용자 %jd는 이름이 없습니다"

#: lib/user.c:356
#, c-format
msgid "group %jd has no name"
msgstr "그룹 %jd는 이름이 없습니다"

#: lib/user.c:364
msgid "user has neither a name nor an UID"
msgstr "사용자는 이름과 UID가 없습니다"

#: lib/user.c:365
msgid "group has neither a name nor a GID"
msgstr "그룹은 이름과 GID가 없습니다"

#: lib/user.c:1311
#, c-format
msgid "Refusing to use dangerous home directory `%s' by default"
msgstr "기본값으로 안전하지 않은 `%s' 홈 디렉토리 사용을 거부합니다  "

#: lib/user.c:2310
#, c-format
msgid "Invalid default value of field %s: %s"
msgstr "%s 란의 기본값이 잘못됨: %s"

#: lib/util.c:300 modules/files.c:374
#, c-format
msgid "error locking file: %s"
msgstr "파일을 잠그는 도중 오류 발생: %s"

#: lib/util.c:704
#, c-format
msgid "couldn't get default security context: %s"
msgstr "기본 보안 문맥을 읽어오지 못했습니다: %s  "

#: lib/util.c:731 lib/util.c:757 lib/util.c:783
#, c-format
msgid "couldn't get security context of `%s': %s"
msgstr "`%s'의 보안 문맥을 읽어오지 못했습니다: %s "

#: lib/util.c:737 lib/util.c:763 lib/util.c:789 lib/util.c:821
#, c-format
msgid "couldn't set default security context to `%s': %s"
msgstr "기본 보안 문맥을 `%s'으로 설정할 수 없습니다: %s"

#: lib/util.c:813
#, c-format
msgid "couldn't determine security context for `%s': %s"
msgstr "`%s'의 보안 문맥을 결정하지 못했습니다: %s "

#: modules/files.c:129 modules/files.c:692 modules/files.c:1585
#: modules/files.c:1920 modules/files.c:1930 modules/files.c:2012
#: modules/files.c:2023 modules/files.c:2089 modules/files.c:2101
#: modules/files.c:2191 modules/files.c:2200 modules/files.c:2255
#: modules/files.c:2264 modules/files.c:2359 modules/files.c:2368
#, c-format
msgid "couldn't open `%s': %s"
msgstr "`%s'(을)를 열 수 없음: %s"

#: modules/files.c:137 modules/files.c:994 modules/files.c:1187
#: modules/files.c:1329
#, c-format
msgid "couldn't stat `%s': %s"
msgstr "`%s'(을)를 표시(stat)할 수 없음: %s"

#: modules/files.c:161
#, c-format
msgid "error creating `%s': %s"
msgstr "`%s'(을)를 생성하는 도중 오류 발생: %s"

#: modules/files.c:169
#, c-format
msgid "Error changing owner of `%s': %s"
msgstr "`%s' 소유자를 변경하는 도중 오류 발생: %s"

#: modules/files.c:175
#, c-format
msgid "Error changing mode of `%s': %s"
msgstr "`%s' 모드 변경 도중 오류 발생: %s"

#: modules/files.c:191
#, c-format
msgid "Error reading `%s': %s"
msgstr "`%s'(을)를 읽는 도중 오류 발생: %s"

#: modules/files.c:206 modules/files.c:217 modules/files.c:305
#: modules/files.c:467
#, c-format
msgid "Error writing `%s': %s"
msgstr "`%s'(을)를 기록하는 도중 오류 발생: %s"

#: modules/files.c:247 modules/files.c:1005 modules/files.c:1195
#: modules/files.c:1338
#, c-format
msgid "couldn't read from `%s': %s"
msgstr "`%s'(을)를 읽을 수 없음: %s"

#: modules/files.c:256
#, c-format
msgid "Invalid contents of lock `%s'"
msgstr ""

#: modules/files.c:261
#, c-format
msgid "The lock %s is held by process %ju"
msgstr ""

#: modules/files.c:269
#, fuzzy, c-format
msgid "Error removing stale lock `%s': %s"
msgstr "%s에서 %s(으)로 이동하는 도중 오류 발생: %s.\n"

#: modules/files.c:297
#, fuzzy, c-format
msgid "error opening temporary file for `%s': %s"
msgstr "`%s' 모드 변경 도중 오류 발생: %s"

#: modules/files.c:321
#, c-format
msgid "Cannot obtain lock `%s': %s"
msgstr ""

#: modules/files.c:434
#, fuzzy, c-format
msgid "Error resolving `%s': %s"
msgstr "`%s'(을)를 읽는 도중 오류 발생: %s"

#: modules/files.c:442
#, fuzzy, c-format
msgid "Error replacing `%s': %s"
msgstr "`%s'(을)를 읽는 도중 오류 발생: %s"

#: modules/files.c:903
#, fuzzy, c-format
msgid "%s value `%s': `\\n' not allowed"
msgstr "%s 값 `%s': `:'은 허용되지 않음 "

#: modules/files.c:910
#, c-format
msgid "%s value `%s': `:' not allowed"
msgstr "%s 값 `%s': `:'은 허용되지 않음 "

#: modules/files.c:1014
msgid "entry already present in file"
msgstr "파일 안에 목록(entry)이 이미 존재합니다"

#: modules/files.c:1021 modules/files.c:1031 modules/files.c:1041
#: modules/files.c:1393 modules/files.c:1401 modules/files.c:1409
#, c-format
msgid "couldn't write to `%s': %s"
msgstr "`%s'에 작성할 수 없음: %s"

#: modules/files.c:1173
#, c-format
msgid "entity object has no %s attribute"
msgstr "entity 개체에 %s 속성이 없습니다"

#: modules/files.c:1215
msgid "entry with conflicting name already present in file"
msgstr "목록에 이미 중복되는 이름을 가진  파일이 존재합니다."

#: modules/files.c:1803
#, fuzzy
msgid "`:' and `\\n' not allowed in encrypted password"
msgstr "`:'은 암호화된 암호에서 허용되지 않음 "

#: modules/files.c:1815 modules/ldap.c:1543 modules/ldap.c:1812
msgid "error encrypting password"
msgstr "암호를 암호화하는 도중 오류 발생"

#: modules/files.c:2517 modules/ldap.c:2410
#, c-format
msgid "the `%s' and `%s' modules can not be combined"
msgstr "'%s', '%s 모듈은 같이 사용될 수 없습니다."

#: modules/files.c:2601 modules/files.c:2679
msgid "not executing with superuser privileges"
msgstr "수퍼유저 권한으로 실행할 수 없습니다"

#: modules/files.c:2692
msgid "no shadow file present -- disabling"
msgstr "현재 섀도우 파일이 없음 -- 불가능"

#: modules/ldap.c:199
msgid "error initializing ldap library"
msgstr "ldap 라이브러리를 초기화하는 도중 오류가 발생했습니다"

#: modules/ldap.c:210
#, c-format
msgid "could not set LDAP protocol to version %d"
msgstr "%d 버전에 LDAP 프로토콜을 설정할 수 없습니다"

#: modules/ldap.c:229
msgid "could not negotiate TLS with LDAP server"
msgstr "LDAP 서버와 TLS 연결할 수 없습니다"

#: modules/ldap.c:424
msgid "could not bind to LDAP server"
msgstr "LDAP 서버와 연결할 수 없습니다"

#: modules/ldap.c:427
#, c-format
msgid "could not bind to LDAP server, first attempt as `%s': %s"
msgstr "LDAP 서버와 연결할 수 없습니다, `%s'로 첫번째 시도: %s"

#: modules/ldap.c:1315
#, c-format
msgid "user object had no %s attribute"
msgstr "사용자 개체에 %s 속성이 없습니다"

#: modules/ldap.c:1324
#, c-format
msgid "user object was created with no `%s'"
msgstr "`%s'(이)가 없는 사용자 개체가 생성되었습니다"

#: modules/ldap.c:1344
#, c-format
msgid "error creating a LDAP directory entry: %s"
msgstr "LDAP 디렉토리 항목을 생성하는 도중 오류 발생: %s"

#: modules/ldap.c:1370 modules/ldap.c:1604
#, c-format
msgid "error modifying LDAP directory entry: %s"
msgstr "LDAP 디렉토리 목록(entry)을 편집하는 도중 오류 발생: %s"

#: modules/ldap.c:1395
#, c-format
msgid "error renaming LDAP directory entry: %s"
msgstr "LDAP 디렉토리 항목의 이름을 변경하는 도중 오류 발생: %s"

#: modules/ldap.c:1440
#, c-format
msgid "object had no %s attribute"
msgstr "개체에 %s 속성이 없습니다"

#: modules/ldap.c:1456
#, c-format
msgid "error removing LDAP directory entry: %s"
msgstr "LDAP 디렉토리 항목을 삭제하는 도중 오류 발생: %s"

#: modules/ldap.c:1506 modules/ldap.c:1521 modules/ldap.c:1635
#: modules/ldap.c:1730
#, c-format
msgid "object has no %s attribute"
msgstr "개체에 %s 속성이 없습니다"

#: modules/ldap.c:1533
msgid "unsupported password encryption scheme"
msgstr "지원되지 않는 암호화 체계입니다  "

#: modules/ldap.c:1658
msgid "no such object in LDAP directory"
msgstr "LDAP 디렉토리 안에 그러한 개체가 없습니다"

#: modules/ldap.c:1670
#, c-format
msgid "no `%s' attribute found"
msgstr "`%s' 속성을 찾을 수 없습니다"

#: modules/ldap.c:1843
#, c-format
msgid "error setting password in LDAP directory for %s: %s"
msgstr "%s의 LDAP 디렉토리 안에 암호를 설정하는 도중 오류 발생: %s"

#: modules/ldap.c:2446
msgid "LDAP Server Name"
msgstr "LDAP 서버 이름"

#: modules/ldap.c:2452
msgid "LDAP Search Base DN"
msgstr "LDAP 검색 기반 DN"

#: modules/ldap.c:2458
msgid "LDAP Bind DN"
msgstr "LDAP Bind DN "

#: modules/ldap.c:2465
msgid "LDAP Bind Password"
msgstr "LDAP Bind 암호"

#: modules/ldap.c:2471
msgid "LDAP SASL User"
msgstr "LDAP SASL 사용자"

#: modules/ldap.c:2478
msgid "LDAP SASL Authorization User"
msgstr "LDAP SASL 권한부여 사용자"

#: modules/sasldb.c:132
#, c-format
msgid "Cyrus SASL error creating user: %s"
msgstr "사용자를 생성하는 중 Cyrus SASL 오류 발생: %s"

#: modules/sasldb.c:136
#, c-format
msgid "Cyrus SASL error removing user: %s"
msgstr "사용자를 삭제하는 중 Cyrus SASL 오류 발생: %s"

#: modules/sasldb.c:503 modules/sasldb.c:511
#, c-format
msgid "error initializing Cyrus SASL: %s"
msgstr "Cyrus SASL를 초기화하는 중 오류 발생: %s"

#: python/admin.c:505
msgid "error creating home directory for user"
msgstr "사용자의 홈 디렉토리를 생성하는 중 오류 발생"

#: python/admin.c:544 python/admin.c:583
msgid "error removing home directory for user"
msgstr "사용자의 홈 디렉토리를 삭제하는 중 오류 발생"

#: python/admin.c:654
msgid "error moving home directory for user"
msgstr "사용자의 홈 디렉토리를 이동하는 중 오류 발생"

#: samples/lookup.c:63
#, c-format
msgid "Error initializing %s: %s\n"
msgstr "%s(을)를 초기화하는 도중 오류 발생: %s\n"

#: samples/lookup.c:76
#, c-format
msgid "Invalid ID %s\n"
msgstr "잘못된 ID %s\n"

#: samples/lookup.c:88
#, c-format
msgid "Searching for group with ID %jd.\n"
msgstr "%jd라는 ID를 가진 그룹을 검색하고 있습니다.\n"

#: samples/lookup.c:92
#, c-format
msgid "Searching for group named %s.\n"
msgstr "%s 그룹을 검색하고 있습니다.\n"

#: samples/lookup.c:99
#, c-format
msgid "Searching for user with ID %jd.\n"
msgstr "ID가 %jd인 사용자를 검색하고 있습니다.\n"

#: samples/lookup.c:103
#, c-format
msgid "Searching for user named %s.\n"
msgstr "%s 사용자를 검색하고 있습니다.\n"

#: samples/lookup.c:117
msgid "Entry not found.\n"
msgstr "목록(Entry)을 찾을 수 없습니다.\n"

#: samples/prompt.c:48
msgid "Prompts succeeded.\n"
msgstr "프롬프트 성공.\n"

#: samples/prompt.c:58
msgid "Prompts failed.\n"
msgstr "프롬프트 실패.\n"

#: samples/testuser.c:76
msgid "Default user object classes:\n"
msgstr "기본 사용자 객체 클래스:\n"

#: samples/testuser.c:82
msgid "Default user attribute names:\n"
msgstr "기본 사용자 속성 이름들:\n"

#: samples/testuser.c:88
msgid "Getting default user attributes:\n"
msgstr "기본 사용자 속성을 읽음:\n"

#: samples/testuser.c:95
msgid "Copying user structure:\n"
msgstr "사용자 구조를 복사함:\n"

#~ msgid "backup file `%s' exists and is not a regular file"
#~ msgstr "백업 파일 `%s' 존재하지만 정규 파일이 아닙니다"

#~ msgid "backup file size mismatch"
#~ msgstr "백업 파일 용량이 일치하지 않음"
